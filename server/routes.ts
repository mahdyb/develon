import { Application } from "express";
import gamesRouter from "./api/controllers/games/router";
import oauthRouter from "./api/controllers/oauth/router";
import usersRouter from "./api/controllers/users/router";

export default function routes(app: Application): void {
  app.use("/api", usersRouter);
  app.use("/api", gamesRouter);
  app.use("/oaut", oauthRouter);
}
