openapi: 3.0.1
info:
  title: Develon
  description: Develon test task
  version: 1.0.0
servers:
- url: /
tags:
- name: Users
  description: Users endpoints
- name: Games
  description: Games endpoints  
- name: Specification
  description: The swagger API specification
paths:
  /api/user:
    get:
      tags:
      - Users
      description:  List of users
      responses:
        200:
          description: Returns all users
          content: {}
    post:
      tags:
      - Users
      description: Register new user
      requestBody:
        description: user
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserBody'
        required: true
      responses:
        200:
          description: Returns tokens
          content: {}
        400:
          description: Returns errors
  /api/me:
    get:
      tags:
      - Users
      description:  Who am I
      responses:
        200:
          description: Returns current user
          content: {}
    post:
      tags:
      - Users
      description: Update the user's information
      requestBody:
        description: user
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserBody'
        required: true
      responses:
        200:
          description: Returns user data
          content: {}
        400:
          description: Returns errors          
  /oaut/token:
    post:
      tags:
      - Users
      description: Login
      requestBody:
        description: login
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/LoginBody'
        required: true
      responses:
        200:
          description: Returns tokens
          content: {}
        400:
          description: Returns errors

  /api/game:
    post:
      tags:
      - Games
      description: Create a new game
      requestBody:
        description: game
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateGameBody'
        required: true
      responses:
        200:
          description: Returns game data
          content: {}
        400:
          description: Returns errors
    get:
      tags:
      - Games
      description: Get all games
      responses:
        200:
          description: Returns all game data
          content: {}
        400:
          description: Returns errors    

  /api/game/{gameId}:  
    put:
      tags:
      - Games
      description: Update a game
      parameters:
        - in: path
          name: gameId
          schema:
            type: integer
          required: true
          description: Numeric ID of the game     
      requestBody:
        description: game
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateGameBody'
        required: true
      responses:
        200:
          description: Returns game data
          content: {}
        400:
          description: Returns errors         
    get:
      tags:
      - Games
      description: View a game
      parameters:
        - in: path
          name: gameId
          schema:
            type: integer
          required: true
          description: Numeric ID of the game
      responses:
        200:
          description: Returns game data
          content: {}
        400:
          description: Returns errors             
    delete:
      tags:
      - Games
      description: Delete a game
      parameters:
        - in: path
          name: gameId
          schema:
            type: integer
          required: true
          description: Numeric ID of the game     
      responses:
        200:
          description: Returns status message
          content: {}
        400:
          description: Returns errors          
  /api/game/{gameId}/join:  
    post:
      tags:
      - Games
      description: Join a game
      parameters:
        - in: path
          name: gameId
          schema:
            type: integer
          required: true
          description: Numeric ID of the game     
      responses:
        200:
          description: Returns game data
          content: {}
        400:
          description: Returns status   
  /api/game/{gameId}/left:  
    delete:
      tags:
      - Games
      description: Left a game
      parameters:
        - in: path
          name: gameId
          schema:
            type: integer
          required: true
          description: Numeric ID of the game     
      responses:
        200:
          description: Returns status
          content: {}
        400:
          description: Returns errors             
  /api/game/{gameId}/invite:  
    post:
      tags:
      - Games
      description: Invite a user to a game
      parameters:
        - in: path
          name: gameId
          schema:
            type: integer
          required: true
          description: Numeric ID of the game     
      requestBody:
        description: user
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InviteBody'
        required: true          
      responses:
        200:
          description: Returns status
          content: {}
        400:
          description: Returns errors            

  /api/game/{gameId}/kick:  
    post:
      tags:
      - Games
      description: Kick a user from a game
      parameters:
        - in: path
          name: gameId
          schema:
            type: integer
          required: true
          description: Numeric ID of the game     
      requestBody:
        description: user
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InviteBody'
        required: true          
      responses:
        200:
          description: Returns status
          content: {}
        400:
          description: Returns errors          

  /api/game/{gameId}/score:  
    post:
      tags:
      - Games
      description: Add to score
      parameters:
        - in: path
          name: gameId
          schema:
            type: integer
          required: true
          description: Numeric ID of the game     
      requestBody:
        description: user
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ScoreBody'
        required: true          
      responses:
        200:
          description: Returns game
          content: {}
        400:
          description: Returns errors            

  /spec:
    get:
      tags:
      - Specification
      responses:
        200:
          description: Return the API specification
          content: {}
components:
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT 
  schemas:
    UserBody:
      title: user
      type: object
      properties:
        name:
          type: string
        email:
          type: string
        password:
          type: string  

    LoginBody:
      title: credentials
      required:
      - username
      - password
      type: object
      properties:
        username:
          type: string
        password:
          type: string   
    
    CreateGameBody:
        title : game props
        required:
          - target_score
        type: object
        properties:
          target_score:
            type: number
    InviteBody:
        title : user props
        required:
          - user_id
        type: object
        properties:
          user_id:
            type: number            
    ScoreBody:
        title : score props
        required:
          - score
        type: object
        properties:
          score:
            type: number            
security:
  - bearerAuth: []