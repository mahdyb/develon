import bodyParser from "body-parser";
import cookieParser from "cookie-parser"; import "reflect-metadata";
import express from "express";
import { Application } from "express";
import http from "http";
import os from "os";
import path from "path";
import {createConnection} from "typeorm";
import l from "./logger";
import installValidator from "./openapi";

const app = express();

export default class ExpressServer {
  constructor() {
      const root = path.normalize(__dirname + "/../..");
      app.set("appPath", root + "client");
      app.use(bodyParser.json({ limit: process.env.REQUEST_LIMIT || "100kb" }));
      app.use(bodyParser.urlencoded({ extended: true, limit: process.env.REQUEST_LIMIT || "100kb" }));
      app.use(cookieParser(process.env.SESSION_SECRET));
      app.use(express.static(`${root}/public`));
  }

  public router(routes: (app: Application) => void): ExpressServer {
    installValidator(app, routes);
    return this;
  }

  public listen(p: string | number = process.env.PORT): Application {
    const welcome = (port) => () => l.info(`up and running in ${process.env.NODE_ENV || "development"} @: ${os.hostname() } on port: ${port}`);
    createConnection().then(async (connection) => {
      l.info("connected to database");
      http.createServer(app).listen(p, welcome(p));
    }).catch((error) => l.error(error));

    return app;
  }
}
