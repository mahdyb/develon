import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";

export default (req: Request, res: Response, next: NextFunction) => {
  const token =  (req.headers.authorization || "").replace("Bearer ", "") || false;
  if (token) {
    jwt.verify(token, (process.env.JWT_SECRET as string) , (err, decoded) => {
        if (err) {
            return res.status(401).json({error: true, message: "Unauthorized access." });
        }
        req[`decoded`] = decoded;
        next();
    });
  } else {
    // if there is no token return an error
    return res.status(403).send({
        error: true,
        message: "No token provided.",
    });
  }
};
