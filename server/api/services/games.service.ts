import {validate} from "class-validator";
import L from "../../common/logger";
import {Game} from "../../entity/Game";
import {Pivot} from "../../entity/Pivot";
import {User} from "../../entity/User";

export class GamesService {

  /**
   * Creates a new game.
   * @param target_score - Game target
   * @param owner - Creator of the game
   * @returns Game object with a single user which is `owner`
   */
  public async create(target_score: number , owner: User): Promise<Game> {
    // game object
    const game = new Game();
    game.target_score = target_score;
    delete owner.pivots;
    game.users = [owner];

    // validate game props
    const errors = await validate(game);
    if (errors.length > 0 ) {
      L.error("game validation failed. errors: ", errors);
      return Promise.reject({errors, status: 400});
    } else {
      L.info("game validation succeed");
      await game.save();
      return Promise.resolve(game);
    }
  }

  /**
   * Updates a game
   * @param game_id - ID of the game
   * @param target_score - Game target
   * @returns Game object with updated target score
   */
  public async update(game_id: number, target_score: number): Promise<Game> {
      // game object
      const game = await Game.findOne({id : game_id});
      game.target_score = target_score;

      // validate game props
      const errors = await validate(game);
      if (errors.length > 0 ) {
        L.error("game validation failed. errors: ", errors);
        return Promise.reject({errors, status: 400});
      } else {
        L.info("game validation succeed");
        await game.save();
        return Promise.resolve(game);
      }
  }

  /**
   * Fethes a game data
   * @param game_id - ID of the game
   * @returns Game object with joined users
   */
  public async single(game_id: number): Promise<Game> {
      const game = await Game.findOne({id : game_id}, { relations: ["users"] });
      if (game && game.id) {
        return Promise.resolve(game);
      }
      return Promise.reject({message : "game not found"});
  }

  /**
   * Deletes a game
   * @param game_id - ID of the game
   * @returns Status message or error
   */
  public async delete(game_id: number): Promise<object> {
      const game = await Game.findOne({id : game_id});
      if (game && game.id) {
        game.remove();
        return Promise.resolve({message : "success"});
      }
      return Promise.reject({message : "game not found"});
  }

  /**
   * Fethes all games
   * @returns All games as an array
   */
  public async all(): Promise<Game[]> {
      const games = await Game.find();
      return Promise.resolve(games);
  }

  /**
   * Adds a user to a game
   * @param game_id - ID of the game
   * @param user_id - ID of the user
   * @returns Status message or error
   */
  public async addUser(game_id: number , user_id: number): Promise<object> {
    const game = await Game.findOne({id : game_id}, { relations: ["users"] });
    const user = User.create({id: user_id});
    await user.reload();
    if (game && game.id && user.email) {
      game.users.push(user);
      await game.save();
      return Promise.resolve({message : "success"});
    }
    return Promise.reject({message : "invalid game or user"});
  }

  /**
   * Removes a user from a game
   * @param game_id - ID of the game
   * @param user_id - ID of the user
   * @returns Status message or error
   */
  public async removeUser(game_id: number , user_id: number): Promise<object> {
    const game = await Game.findOne({id : game_id}, { relations: ["users"] });
    const user = User.create({id: user_id});
    await user.reload();
    if (game && game.id && user.email) {
      const index = game.users.findIndex((member) => member.id === user.id);
      game.users.splice(index, 1);
      await game.save();
      return Promise.resolve({message : "success"});
    }
    return Promise.reject({message : "invalid game or user"});
  }

  /**
   * Sets an score to a game and determinates winner if available
   * @param game_id - ID of the game
   * @param user_id - ID of the user
   * @param score   - Score of the current round
   * @returns Pivot object or error
   */
  public async score(game_id: number , user_id: number , score: number): Promise<Pivot> {
      const game = await Game.findOne({id : game_id}, { relations: ["users"] });
      if (game && game.id) {
        const user = await User.findOne({id: user_id}, {relations: ["pivots"]});
        const pivot = new Pivot();
        pivot.score = score;
        pivot.game_id = game_id;
        await pivot.save();
        user.pivots.push(pivot);
        await user.save();

        // determinate if the current user is winner or not
        const total_score = await Pivot.createQueryBuilder("pivot").select("SUM(score)", "sum")
        .where("user_id = :user_id", { user_id})
        .where("game_id = :game_id", { game_id})
        .getRawOne();

        if (total_score.sum >= game.target_score && game.winner === null) {
          L.info(`game #${game_id} has a winner now !`);
          game.winner = user_id;
          await game.save();
        }

        return Promise.resolve(pivot);
      }
      return Promise.reject({message : "invalid game"});
    }

}

export default new GamesService();
