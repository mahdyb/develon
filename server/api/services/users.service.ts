import bcrypt from "bcrypt";
import {validate} from "class-validator";
import jwt from "jsonwebtoken";
import jwtDecode from "jwt-decode";
import L from "../../common/logger";
import {User} from "../../entity/User";

interface IToken {
  token_type: string;
  expires_in: number;
  access_token: string;
  refresh_token: string;
}

export class UsersService {

  /**
   * Fethes all users
   * @returns All users as an array
   */
  public async all(): Promise<User[]> {
    const users = await User.find({select:
      ["id", "email", "name", "created_at", "updated_at"]});
    return Promise.resolve(users);
  }

  /**
   * Fethes a user data
   * @param email - Email address of the user
   * @returns User object
   */
  public async single(email: string): Promise<User> {
    const user = await User.findOne({email}, {select:
    ["id", "email", "name", "email_verified_at", "updated_at", "created_at"]});
    return Promise.resolve(user);
  }

  /**
   * Creates a new user.
   * @param email - Email address of the user
   * @param password - User's password
   * @param name - Name of the user
   * @returns User object without password
   */
  public async create(email: string, password: string , name: string): Promise<User> {
    const user = new User();
    user.email = email;
    user.name = name;
    user.password = password;

    // validate user props
    const errors = await validate(user);
    if (errors.length > 0 ) {
      L.error("validation failed. errors: ", errors);
      return Promise.reject({errors, status: 400});
    } else {
      L.info("validation succeed");

      // hash password using bcrypt
      user.password = await bcrypt.hash(user.password, 12);
      await user.save();

      // remove password from final output
      delete user.password;
      return Promise.resolve(user);
    }
  }

  /**
   * Updates a user data.
   * @param user - Original user object
   * @param email - Updated email address
   * @param name - Updated name
   * @param password - Updated password
   * @returns Updated user object without password
   */
  public async update(user: User, email: string, name: string, password: string ): Promise<User> {
    user.email = email || user.email;
    user.name = name || user.name;
    user.password = password  || user.password;
    delete user.pivots;

    // validate user props
    const errors = await validate(user);
    if (errors.length > 0 ) {
      L.error("validation failed. errors: ", errors);
      return Promise.reject({errors, status: 400});
    } else {
      L.info("validation succeed");
      if (password) {user.password = await bcrypt.hash(password, 12); }
      await user.save();

      // remove password from final output
      delete user.password;
      return Promise.resolve(user);
    }
  }

  /**
   * Authenticate a user
   * @param username - Email address of the user
   * @param password - User's password
   * @returns JWT token of the loggined user or error message
   */
  public async login(username: string , password: string): Promise<IToken> {

    const user = await User.createQueryBuilder("user").where("user.email = :username", { username }).addSelect("user.password").getOne();
    if (user && user.password) {
      // compare password
      const match = await bcrypt.compare(password, user.password);
      if (match) {
        // sing user data and returns jwt
        const userData = {id: user.id, name: user.name, email: user.email};
        const token = jwt.sign(userData, process.env.JWT_SECRET, { expiresIn: process.env.JWT_TOKEN_LIFE});
        const refreshToken = jwt.sign(userData, process.env.JWT_REFRESH_SECRET, { expiresIn: process.env.JWT_REFRESH_TOKEN_LIFE});
        const expiresIn = jwtDecode(token).exp;

        return Promise.resolve({
          token_type: "Bearer",
          expires_in: expiresIn,
          access_token: token,
          refresh_token: refreshToken,
        });
      }
      return Promise.reject({errors: "auth faild"});
    }
    return Promise.reject({errors: "user not found"});
  }
}

export default new UsersService();
