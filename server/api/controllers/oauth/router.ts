import express from "express";
import controller from "./controller";
export default express.Router()
     // Login
    .post("/token", controller.login);
