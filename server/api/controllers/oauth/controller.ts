import { NextFunction, Request, Response } from "express";
import UsersService from "../../services/users.service";

export class Controller {
  public login(req: Request, res: Response, next: NextFunction): void {
    UsersService.login(req.body.username, req.body.password).then(
      (user) => res.status(201).json(user),
      (error) => next(error),
    );
  }
}
export default new Controller();
