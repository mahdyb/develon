import { NextFunction, Request, Response } from "express";
import {User} from "../../../entity/User";
import GamesService from "../../services/games.service";

export class Controller {

  public async create(req: Request, res: Response, next: NextFunction): Promise<void> {
    const owner = User.create(req[`decoded`]);
    await owner.reload();
    GamesService.create(req.body.target_score, owner).then(
      (game) => res.status(201).json(game),
      (error) => next(error),
    );
  }

  public async update(req: Request, res: Response, next: NextFunction): Promise<void> {
    const game_id = req.params.game_id;
    GamesService.update(game_id, req.body.target_score).then(
      (game) => res.status(200).json(game),
      (error) => next(error),
    );
  }

  public async view(req: Request, res: Response, next: NextFunction): Promise<void> {
    const game_id = req.params.game_id;
    GamesService.single(game_id).then(
      (game) => res.status(200).json(game),
      (error) => next(error),
    );
  }

  public async delete(req: Request, res: Response, next: NextFunction): Promise<void> {
    const game_id = req.params.game_id;
    GamesService.delete(game_id).then(
      (game) => res.status(200).json(game),
      (error) => next(error),
    );
  }

  public async viewAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    GamesService.all().then(
      (games) => res.status(200).json(games),
      (error) => next(error),
    );
  }

  public async join(req: Request, res: Response, next: NextFunction): Promise<void> {
    const game_id = req.params.game_id;
    const user_id = req[`decoded`].id;
    GamesService.addUser(game_id, user_id).then(
      (result) => res.status(200).json(result),
      (error) => next(error),
    );
  }

  public async invite(req: Request, res: Response, next: NextFunction): Promise<void> {
    const game_id = req.params.game_id;
    const user_id = req.body.user_id;
    GamesService.addUser(game_id, user_id).then(
      (result) => res.status(200).json(result),
      (error) => next(error),
    );
  }

  public async kick(req: Request, res: Response, next: NextFunction): Promise<void> {
    const game_id = req.params.game_id;
    const user_id = req.body.user_id;
    GamesService.removeUser(game_id, user_id).then(
      (result) => res.status(200).json(result),
      (error) => next(error),
    );
  }

  public async leave(req: Request, res: Response, next: NextFunction): Promise<void> {
    const game_id = req.params.game_id;
    const user_id = req[`decoded`].id;
    GamesService.removeUser(game_id, user_id).then(
      (result) => res.status(200).json(result),
      (error) => next(error),
    );
  }

  public async score(req: Request, res: Response, next: NextFunction): Promise<void> {
    const game_id = req.params.game_id;
    const user_id = req[`decoded`].id;
    const score = req.body.score;
    GamesService.score(game_id, user_id , score).then(
      (result) => res.status(200).json(result),
      (error) => next(error),
    );
  }

}
export default new Controller();
