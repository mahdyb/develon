import express from "express";
import isAuthenticated from "../../middlewares/token.checker";
import controller from "./controller";
export default express.Router()
     // Create a new game
    .post("/game", isAuthenticated, controller.create)
     // Update a game
    .put("/game/:game_id", isAuthenticated, controller.update)
     // View a game
    .get("/game/:game_id", isAuthenticated, controller.view)
     // Delete a game
    .delete("/game/:game_id", isAuthenticated, controller.delete)
     // List Of Games
    .get("/game", isAuthenticated, controller.viewAll)
     // Join a game
    .post("/game/:game_id/join", isAuthenticated, controller.join)
     // Invite a user to a game
    .post("/game/:game_id/invite", isAuthenticated, controller.invite)
     // Kick a user from a game
    .post("/game/:game_id/kick", isAuthenticated, controller.kick)
     // leave a game
    .delete("/game/:game_id/left", isAuthenticated, controller.leave)
     // set score
    .post("/game/:game_id/score", isAuthenticated, controller.score);
