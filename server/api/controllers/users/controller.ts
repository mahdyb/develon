import { NextFunction, Request, Response } from "express";
import {User} from "../../../entity/User";
import UsersService from "../../services/users.service";

export class Controller {
  public all(req: Request, res: Response): void {
    UsersService.all().then((r) => res.json(r));
  }

  public create(req: Request, res: Response, next: NextFunction): void {
    UsersService.create(req.body.email, req.body.password , req.body.name).then(
      (user) => res.status(201).json(user),
      (error) => next(error),
    );
  }

  public async update(req: Request, res: Response, next: NextFunction): Promise<void> {
    const user = await User.createQueryBuilder("user").where("user.id = :id", { id: req[`decoded`].id }).addSelect("user.password").getOne();
    UsersService.update(user,  req.body.email, req.body.name , req.body.password).then(
      (result) => res.status(200).json(result),
      (error) => next(error),
    );
  }

  public getCurrentUser(req: Request, res: Response, next: NextFunction): void {
    // get email from jwt verified payload
    const email = req[`decoded`].email;

    UsersService.single(email).then(
      (user) => res.status(200).json(user),
      (error) => next(error),
    );

  }
}
export default new Controller();
