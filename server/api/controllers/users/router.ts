import express from "express";
import isAuthenticated from "../../middlewares/token.checker";
import controller from "./controller";
export default express.Router()
     // Register
    .post("/user", controller.create)
     // List of users
    .get("/user", controller.all)
     // Who am I?
    .get("/me", isAuthenticated, controller.getCurrentUser)
     // Update the user's information
    .post("/me", isAuthenticated, controller.update);
