import {BaseEntity, Column, CreateDateColumn, Entity,
ManyToOne , PrimaryGeneratedColumn , UpdateDateColumn} from "typeorm";
import {User} from "./User";

@Entity()
export class Pivot extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public score: number;

    @Column()
    public game_id: number;

    @ManyToOne((type) => User, (user) => user.pivots)
    public user: User;

    @UpdateDateColumn()
    public updated_at: Date;

    @CreateDateColumn()
    public created_at: Date;

}
