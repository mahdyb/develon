import {IsEmail, MinLength} from "class-validator";
import {BaseEntity, Column, CreateDateColumn, Entity, OneToMany , PrimaryGeneratedColumn , UpdateDateColumn} from "typeorm";
import {Pivot} from "./Pivot";

@Entity()
export class User extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column({unique: true})
    @IsEmail()
    public email: string;

    @Column({ select: false })
    @MinLength(5)
    public password: string;

    @CreateDateColumn()
    public email_verified_at: Date;

    @OneToMany((type) => Pivot, (pivot) => pivot.user, {eager: true})
    public pivots: Pivot[];

    @UpdateDateColumn()
    public updated_at: Date;

    @CreateDateColumn()
    public created_at: Date;

}
