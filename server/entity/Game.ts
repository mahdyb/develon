import {IsPositive} from "class-validator";
import {BaseEntity, Column, CreateDateColumn, Entity, JoinColumn, JoinTable,
ManyToMany, OneToOne , PrimaryGeneratedColumn , UpdateDateColumn} from "typeorm";
import {User} from "./User";

@Entity()
export class Game extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    @IsPositive()
    public target_score: number;

    @UpdateDateColumn()
    public updated_at: Date;

    @CreateDateColumn()
    public created_at: Date;

    @Column({nullable: true})
    public winner: number;

    @ManyToMany((type) => User)
    @JoinTable()
    public users: User[];

}
