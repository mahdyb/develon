import { expect } from "chai";
import "mocha";
import request from "supertest";
import Server from "../server";

const server = request(Server);
let token = {};

before((done) => {
  server
  .post("/api/user")
  .send(
    {
      name: "Test User",
      email: "test@example.com",
      password: "s@meRandomString",
    },
  ).end(() => {
    server
    .post("/oaut/token")
    .send(
      {
        username: "test@example.com",
        password: "s@meRandomString",
      },
    ).end((err, response) => {
      expect(response.body)
      .to.be.an("object")
      .that.has.property("access_token");
      token = {authorization: `Bearer ${response.body.access_token}`};
      done();
    });
  });
});

describe("Users", () => {

  it("should get all users", () =>
  server
    .get("/api/user")
    .expect("Content-Type", /json/)
    .then((r) => {
      expect(r.body)
        .to.be.an("array");
    }));

  it("should get user information", () =>
      server
        .get("/api/me")
        .set(token)
        .expect("Content-Type", /json/)
        .then((r) => {
          expect(r.body)
            .to.be.an("object")
            .that.has.property("email")
            .equal("test@example.com");
        }));

  it("should update user information", () =>
        server
          .post("/api/me")
          .set(token)
          .send({
            name : "new name",
          })
          .expect("Content-Type", /json/)
          .then((r) => {
            expect(r.body)
              .to.be.an("object")
              .that.has.property("name")
              .equal("new name");
          }));

});
