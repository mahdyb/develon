import { expect } from "chai";
import "mocha";
import request from "supertest";
import Server from "../server";

const server = request(Server);
let token = {};
let user_id = 0;
let game_id = 0;

before((done) => {
  server
  .post("/api/user")
  .send(
    {
      name: "Test User",
      email: "test@example.com",
      password: "s@meRandomString",
    },
  ).end((err, response) => {
    server
    .post("/oaut/token")
    .send(
      {
        username: "test@example.com",
        password: "s@meRandomString",
      },
    ).end((err, response) => {
      expect(response.body)
      .to.be.an("object")
      .that.has.property("access_token");
      token = {authorization: `Bearer ${response.body.access_token}`};
      done();
    });
  });
});

describe("Games", () => {

  it("should get all games", () =>
  server
    .get("/api/game")
    .set(token)
    .expect("Content-Type", /json/)
    .then((r) => {
      expect(r.body)
        .to.be.an("array");
    }));

  it("should create a new game", () =>
        server
          .post("/api/game")
          .set(token)
          .send({
            target_score: 150,
          })
          .expect("Content-Type", /json/)
          .then((r) => {
            user_id = r.body.users[0].id;
            game_id = r.body.id;
            expect(r.body)
              .to.be.an("object")
              .that.has.property("target_score")
              .equal(150);
          }));

  it("should update a game", () =>
        server
          .put(`/api/game/${game_id}`)
          .set(token)
          .send({
            target_score: 200,
          })
          .expect("Content-Type", /json/)
          .then((r) => {
            expect(r.body)
              .to.be.an("object")
              .that.has.property("target_score")
              .equal(200);
          }));

  it("should receive a game information", () =>
          server
            .get(`/api/game/${game_id}`)
            .set(token)
            .expect("Content-Type", /json/)
            .then((r) => {
              expect(r.body)
                .to.be.an("object")
                .that.has.property("target_score")
                .equal(200);
              expect(r.body)
                .to.be.an("object")
                .that.has.property("winner")
                .equal(null);
            }));

  it("should remove current user from a game", () =>
            server
              .delete(`/api/game/${game_id}/left`)
              .set(token)
              .expect("Content-Type", /json/)
              .then((r) => {
                expect(r.body)
                  .to.be.an("object")
                  .that.has.property("message")
                  .equal("success");
              }));

  it("should join current user to a game", () =>
              server
                .post(`/api/game/${game_id}/join`)
                .set(token)
                .expect("Content-Type", /json/)
                .then((r) => {
                  expect(r.body)
                    .to.be.an("object")
                    .that.has.property("message")
                    .equal("success");
                }));

  it("should kick a user from a game", () =>
                server
                  .post(`/api/game/${game_id}/kick`)
                  .set(token)
                  .send({
                    user_id,
                  })
                  .expect("Content-Type", /json/)
                  .then((r) => {
                    expect(r.body)
                      .to.be.an("object")
                      .that.has.property("message")
                      .equal("success");
                  }));

  it("should invite a user to a game", () =>
                  server
                    .post(`/api/game/${game_id}/invite`)
                    .set(token)
                    .send({
                      user_id,
                    })
                    .expect("Content-Type", /json/)
                    .then((r) => {
                      expect(r.body)
                        .to.be.an("object")
                        .that.has.property("message")
                        .equal("success");
                    }));

  it("should add score for current user", () =>
                    server
                      .post(`/api/game/${game_id}/score`)
                      .set(token)
                      .send({
                        score: 200,
                      })
                      .expect("Content-Type", /json/)
                      .then((r) => {
                        expect(r.body)
                          .to.be.an("object")
                          .that.has.property("score")
                          .equal(200);
                      }));

  it("should set winner if score reached the target", () =>
                    server
                    .get(`/api/game/${game_id}`)
                    .set(token)
                    .expect("Content-Type", /json/)
                    .then((r) => {
                      expect(r.body)
                        .to.be.an("object")
                        .that.has.property("winner")
                        .equal(user_id);
                    }));

  it("should remove a game", () =>
                    server
                      .delete(`/api/game/${game_id}`)
                      .set(token)
                      .expect("Content-Type", /json/)
                      .then((r) => {
                        expect(r.body)
                          .to.be.an("object")
                          .that.has.property("message")
                          .equal("success");
                      }));

});
