module.exports = {
   type: "postgres",
   host: process.env.DB_HOST,
   port: process.env.DB_PORT,
   username: process.env.DB_USER,
   password: process.env.DB_PASS,
   database: process.env.DB_NAME,
   synchronize: true,
   logging: false,
   entities: [
      "server/entity/**/*.ts",
   ],
   migrations: [
      "server/entity/**/*.ts",
   ],
   subscribers: [
      "server/entity/**/*.ts",
   ],
   cli: {
      entitiesDir: "server/entity",
      migrationsDir: "server/entity",
      subscribersDir: "server/entity",
   },
};
